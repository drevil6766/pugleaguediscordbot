const d = require("./dbreqs.js");
const u = require("./utils.js");
const {Pickup, Guild} = require("./structs.js");

//TODO this function not completed. Expect it to crash
function set(guild, setting, value, cb){
	//for common spellings that arn't actually the setting value
	switch(setting){
		case 'maps':
			setting = 'mappool';
	}
	if (guild.hasOwnProperty(setting)) {
		switch(setting){
			//Unmodifyables
			case 'guildId': case 'channelids':
				cb(`Cannot modify ${setting}`); break;
			case 'regionid':
				cb("disabled atm"); break;
			//Involving Roles
			case 'whiterole': case 'blackrole': case 'adminrole': case 'modrole':
				cb("unimpemented"); break;
			//Involving lists of strings
			case 'mappool':
				d.setGuild(guild.guildid,setting,value, function(bool){
					if(bool){
						value = value.split(',');
						guild[setting] = value;
						cb(`Successfully set ${setting} to ${value}`);
					}else{
						cb(`Failed to set ${setting} to ${value}`);
					}
				});
				break;
			//Value must be INT in [0,1]
			case 'pickteams':
				value = parseInt(value);
				if(Number.isInteger(value) && value>=0 && value<=1){
					d.setGuild(guild.guildid,setting,value, function(bool){
						if(bool){
							guild[setting] = value;
							cb(`Successfully set ${setting} to ${value}`);
						}else{
							cb(`Failed to set ${setting} to ${value}`);
						}
					});
				}else cb(`Value ${value} is not valid for ${setting}`);
				break;
			//INT >= 1
			case 'teamsize':
				value = parseInt(value);
				if(Number.isInteger(value) && value>=1){
					d.setGuild(guild.guildid,setting,value, function(bool){
						if(bool){
							guild[setting] = value;
							cb(`Successfully set ${setting} to ${value}`);
						}else{
							cb(`Failed to set ${setting} to ${value}`);
						}
					});
				}else cb(`Value ${value} is not valid for ${setting}`);
				break;
			case 'ranked':
				value = parseInt(value);
				if(value==1||value==0){
					d.setGuild(guild.guildid,setting,value, function(bool){
						if(bool){
							guild[setting] = value;
							cb(`Successfully set ${setting} to ${value}`);
						}else{
							cb(`Failed to set ${setting} to ${value}`);
						}
					});
				}else cb(`Value ${value} is not valid for ${setting}`);
				break;
			//all other cases
			default:
				cb(`Setting ${setting} is either unimplemented or blocked`);
				break;
		}
	}else cb(`${setting} is not a valid setting (Did you spell it right?)`);
}

function printConfig(guild, message){
	outMessage = "Config:";
	for(p in guild.pickups){
		for (var key in guild.pickups[p]) {
			if (guild.pickups[p].hasOwnProperty(key)) {
				if (typeof guild.pickups[p][key] !== 'function') {
					outMessage+="\n"+key+": "+guild.pickups[p][key];
				}
			}
		}
	}
	u.dm(message, outMessage, "Printed all Configs");
}

function createPickup(name, pickupid, discordid, leagueid, g){
	newPickup = new Pickup({});

	newPickup.name = name;
	newPickup.pickupid = pickupid;
	newPickup.discordid = discordid;
	newPickup.leagueid = leagueid;

	newPickup.whiterole = g.whiterole;
	newPickup.blackrole = g.blackrole;
	newPickup.mappool = g.mappool;
	newPickup.pickteams = g.pickteams;
	newPickup.ranked = g.ranked;
	newPickup.teamsize = g.teamsize;
	newPickup.placementgames = g.placementgames;
	newPickup.mappickbans = g.mappickbans;
	newPickup.openhours = g.openhours;
	newPickup.bannedclasses = g.bannedclasses;

	newPickup.s_totalgames = 0;
	newPickup.s_draws = 0;
	newPickup.s_mostwonplayer = null;
	newPickup.s_mostwonplayerinfo = null;
	newPickup.s_mostwonteam = null;
	newPickup.s_mostwonteaminfo = null;
	newPickup.s_mostlostplayer = null;
	newPickup.s_mostlostplayerinfo = null;
	newPickup.s_mostlostteam = null;
	newPickup.s_mostlostteaminfo = null;
	newPickup.s_fairestgame = null;
	newPickup.s_fairestgameinfo = null;
	newPickup.s_unfairestgame = null;
	newPickup.s_unfairestgameinfo = null;
	newPickup.s_upset = null;
	newPickup.s_upsetinfo = null;
	newPickup.s_highestgame = null;
	newPickup.s_highestgameinfo = null;
	newPickup.s_lowestgame = null;
	newPickup.s_lowestgameinfo = null;
	newPickup.s_longestwinstreak = 0;
	newPickup.s_longestlosestreak = 0;

	return newPickup;
}

function addPickup(guild, row){
	guild.pickups[row.pickupid] = new Pickup(row, guild);
}

function createGuild(guildId, overlord, channelId) {
	newGuild = new Guild({});

	newGuild.guildid = guildId;
	newGuild.regionid = null;

	newGuild.whiterole = null;
	newGuild.blackrole = null;
	newGuild.mappool = [];
	newGuild.pickteams = 0;
	newGuild.ranked = 0;
	newGuild.teamsize = 4;
	newGuild.placementgames = 10;
	newGuild.mappickbans = 0;
	newGuild.openhours = null;
	newGuild.bannedclasses = 0;

	newGuild.overlordid = overlord;
	newGuild.adminrole = null;
	newGuild.modrole = null;
	newGuild.readytime = 0;
	newGuild.assignblackrole = 0;
	newGuild.channelids = [channelId];
	newGuild.verifychannel = null;
	newGuild.promotedelay = 1800;
	newGuild.waitingroom = null;

	return newGuild;
}

function listNoadds(message, guildId,number=1, bans=false){
	if(isNaN(number)){
		u.reply(message, "Not a valid pagenumber");
		return;
	}else{
		d.getInfractions(guildId, number=1, bans, function(er, rows) {
			u.reply(message, u.printNoadds(rows, number), `Printed Noadds`);
		});
	}
}

module.exports = {
	set,
	printConfig,
	createPickup,
	addPickup,
	createGuild,
	listNoadds
}
