const {Player, Match} = require('./structs.js');
const u = require('./utils.js');
const rs = require('./ranksys');
const d = require("./dbreqs.js");
const r = require("openskill");

//will return true if we made a match
function addQueue(guild, message, authorId, args, nextmatchid){
	[match,p] = playersMatch(guild, authorId);
	if(match!==null){
		u.reply(message, `You are currently in match ${match}`);
		return false;
	}
	added = false;
	for(p in guild.pickups){
		if (args.length > 1 && guild.pickups[p].name != args[1]) continue;
		if(!(guild.pickups[p].queue.includes(authorId))){
			added = true;
			guild.pickups[p].queue.push(authorId);

			//adding (iformation/new players) to active players
			active = guild.active;
			leagueid = guild.pickups[p].leagueid;
			regionid = guild.regionid;
			inActive = !active.hasOwnProperty(authorId);
			if(inActive){
				active[authorId] = new Player();
				rs.getLeague(active[authorId], authorId, leagueid); //MAY NEED A PROMISE FOR THESE
				rs.getLeague(active[authorId], authorId, regionid);
				active[authorId].queuedAt = Date.now();
			}
			nick = message.member.nickname;
			if(nick===null) nick = message.author.username;
			active[authorId].nickname = nick;

			teamsize = guild.pickups[p].teamsize;
			if (guild.pickups[p].queue.length >= teamsize*2) {
				//Match Creation
				players = {};
				for(let i=0;i<guild.pickups[p].queue.length;i++){
					player = guild.pickups[p].queue[i];
					players[player] = guild.active[player];
				}
				createMatch(guild.pickups[p],nextmatchid, players, nextmatchid)

				u.reply(message, u.matchString(guild.pickups[p].matches[nextmatchid]), `Start Match [${nextmatchid}]`, true);

				//updating all other values that are impacted by match creation
				//for example, the queue is now emptied:
				for(let i=0;i<guild.pickups[p].teamsize;i++){
					p1 = guild.pickups[p].matches[nextmatchid].t1[i];
					p2 = guild.pickups[p].matches[nextmatchid].t2[i];
					guild.active[p1].matchId = nextmatchid;
					guild.active[p2].matchId = nextmatchid;
					removeQueue(guild,message,p1,[],true);
					removeQueue(guild,message,p2,[],true);
				}
				guild.pickups[p].s_totalgames++;
				return true;
			}
			printQueue(guild,message);
			break;
		}
	}
}

function printQueue(guild, message) {
	if(guild.pickups.length<1) u.reply(message, "No pickups created");
	outMessage = ``;
	for(p in guild.pickups){
		outMessage += `${guild.pickups[p].name}: [` ;
		for(let i=0;i<guild.pickups[p].queue.length;i++){
			outMessage+=`${guild.active[guild.pickups[p].queue[i]].nickname}`;
			if(i<guild.pickups[p].queue.length-1) outMessage+=',';
		}
		outMessage += `]\n`;
	}
	u.reply(message, outMessage, "Printed queues");
}

function removeQueue(guild, message, authorId, args, silent=false){
	var removed = false;
	for(p in guild.pickups){
		if (guild.pickups[p].queue.length ===0) continue;
		if(args.length>1 && guild.pickups[p].name != args[1]) continue;
		if(guild.pickups[p].queue.length ===1){
			if(guild.pickups[p].queue[0] ===authorId) {
				guild.pickups[p].queue = [];
				if(guild.active[authorId].matchId = null) delete guild.active[authorId];
				else guild.active[authorId].queuedAt = null;
				removed = true;
			}
		}else{
			newQueue = new Array(guild.pickups[p].queue.length-1);
			found = false;
			for(let i=0;i<guild.pickups[p].queue.length-1;i++){
				if(guild.pickups[p].queue[i]===authorId) found=true;
				if(found) newQueue[i] = guild.pickups[p].queue[i+1];
				else newQueue[i] = guild.pickups[p].queue[i];
			}
			if(found) {
				guild.pickups[p].queue = newQueue;
				if(guild.active[authorId].matchId = null) delete guild.active[authorId];
				else guild.active[authorId].queuedAt = null;
				removed = true;
			}
		}
	}
	if(silent) return;
	if(removed) printQueue(guild, message);
	else u.reply(message, `Didn't find you in queue`);
}

function playersMatch(guild, id) {
	for(p in guild.pickups){
		for(m in guild.pickups[p].matches){
			if(guild.pickups[p].matches[m].t1.includes(id)
				|| guild.pickups[p].matches[m].t2.includes(id)){
				return [p,m];
			}
		}
	}
	return [null,null];
}

function printMatches(guild, message) {
	outMessage = "Matches:";
	for(p in guild.pickups){
		for(m in guild.pickups[p].matches){
			outMessage+="\n"+u.matchString(guild.pickups[p].matches[m],true);
		}
	}
	u.reply(message, outMessage, "Printed all Matches");
}

function createMatch(pickup, matchid, players) {
	m = new Match(matchid, pickup.pickupid, players, 2);
	m.t1 = new Array(pickup.teamsize);
	m.t2 = new Array(pickup.teamsize);
	t1count = 0;
	t2count = 0;
	m.scratch = Object.keys(players);
	//TODO: implement proper way of deciding who gets in queue
	m.scratch = m.scratch.slice(0,pickup.teamsize*2);
	for(let i=0;i<pickup.teamsize*2;i++){
		if(t1count>=pickup.teamsize) m.t2[t2count++] = m.scratch[i];
		else if(t2count>=pickup.teamsize) m.t1[t1count++] = m.scratch[i];
		else if(Math.round(Math.random())) m.t2[t2count++] = m.scratch[i];
		else m.t1[t1count++] = m.scratch[i];
	}
	if(pickup.mappool.length>0) m.map = pickup.mappool[u.randInt(0,pickup.mappool.length-1)];
	pickup.matches[matchid] = m;
	//TODO: players that didn't get in match get pick back in queue
	pickup.queue = [];
}

function printMatch(guild, message, args) {
	outMessage = "";

	for(p in guild.pickups){
		for(m in guild.pickups[p].matches){
			if(
				(args.length<2 && 
					(guild.pickups[p].matches[m].t1.includes(message.author.id) 
					|| guild.pickups[p].matches[m].t2.includes(message.author.id)))
				|| (args[1]==m)
			){
				u.reply(message, u.matchString(guild.pickups[p].matches[m]), `Printed Match ${m}`);
				return;
			}
		}
	}
	u.reply(message, `No match found`);
}

function report(guild, message, outcome){
	/* l = loss
	 * c = cancel
	 * d = draw
	 */
	[p,m] = playersMatch(guild, message.author.id);
	if(m===null){
		u.reply(message, "You are not in a match");
		return;
	}
	if(guild.pickups[p].matches[m].state!==2){
		u.reply(message, "Match is not awaiting result");
		return;
	}
	team = -1;
	if(guild.pickups[p].matches[m].t1[0]==message.author.id) team=0;
	else if(guild.pickups[p].matches[m].t2[0]==message.author.id) team=1;
	else {
		u.reply(message, "You are not a captain");
		return;
	}
	switch(outcome){
		case 'l':
			guild.pickups[p].matches[m].result = `${2-team}`;
			finishMatch(guild, message, p,m);
			break;
		case 'c':
			if(guild.pickups[p].matches[m].capReport[1-team]!=='c'){
				guild.pickups[p].matches[m].capReport[team] = 'c';
				u.reply(message, `Waiting on other captain to !rc`);
			}else{
				guild.pickups[p].matches[m].result = 'c';
				finishMatch(guild, message, p,m);
			}
			break;
		case 'd':
			if(guild.pickups[p].matches[m].capReport[1-team]!=='d'){
				guild.pickups[p].matches[m].capReport[team] = 'd';
				u.reply(message, `Waiting on other captain to !rd`);
			}else{
				guild.pickups[p].matches[m].result = 'd';
				guild.pickups[p].s_draws++;
				finishMatch(guild, message, p,m);
			}
			break;
	}
}

function finishMatch(guild, message, p, m){
	result = guild.pickups[p].matches[m].result;
	outMessage = `Match ${m} ended: Team 1 `;
	score = [0,0]
	switch(result){
		case 'c':
			outMessage = `Match ${m} Canceled`;
			break;
		case 'd':
			outMessage+= `== Team 2`;
			break;
		case '1':
			outMessage+= `> Team2`;
			score[0]=1;
			break;
		case "2":
			outMessage+= `< Team2`;
			score[1]=1;
			break;
	}

	match = guild.pickups[p].matches[m];
	s_totalgames = guild.pickups[p].s_totalgames;
	s_draws = guild.pickups[p].s_draws;
	if(guild.ranked==0) match.result = 'c';//unranked = every match just auto cancels once done

	if(result!=='c'){
		var ranks;
		leagueid = guild.pickups[p].leagueid;
		regionid = guild.regionid;
		lid = [leagueid,regionid];
		t1 = match.t1;
		t2 = match.t2;

		l = match.t1.length;
		t1r = new Array(l);
		t2r = new Array(l);
		t1l = new Array(l);
		t2l = new Array(l);
		players = match.players;
		placementgames = guild.pickups[p].placementgames;
		for(let i=0;i<l;i++){
			p1 = t1[i];
			t1r[i] = players[p1].leagues[regionid].rank;
			t1l[i] = players[p1].leagues[leagueid].rank;
			p2 = t2[i];
			t2r[i] = players[p2].leagues[regionid].rank;
			t2l[i] = players[p2].leagues[leagueid].rank;
			for(let j=0;j<2;j++){
				players[p1].leagues[lid[j]].wins += score[0];
				players[p1].leagues[lid[j]].losses += score[1];
				if(score[0]==score[1]) players[p1].leagues[lid[j]].draws++;
				if(players[p1].leagues[lid[j]].streak>0 && score[1]==0) players[p1].leagues[lid[j]].streak+=score[0];
				else if(players[p1].leagues[lid[j]].streak<0 &&score[0]==0) players[p1].leagues[lid[j]].streak-=score[1];
				else players[p1].leagues[lid[j]].streak=score[0]-score[1];

				players[p2].leagues[lid[j]].wins += score[1];
				players[p2].leagues[lid[j]].losses += score[0];
				if(players[p2].leagues[lid[j]].streak>0 && score[0]==0) players[p2].leagues[lid[j]].streak+=score[1];
				else if(players[p2].leagues[lid[j]].streak<0 &&score[1]==0) players[p2].leagues[lid[j]].streak-=score[0];
				else players[p2].leagues[lid[j]].streak=score[1]-score[0];
			}
		}
		[o1r, o2r] = [null,null];
		[o1l, o2l] = [null,null];
		let promise = new Promise(function(resolve,reject) {
			if(result=='d'){//it doesn't like draws with score [0,0] for some reason, need openskill to patch
				[o1r, o2r] = r.rate([t1r, t2r], {score:[1,1]});
				[o1l, o2l] = r.rate([t1l, t2l], {score:[1,1]});
				resolve();
			}else{
				[o1r, o2r] = r.rate([t1r, t2r], {score:score});
				[o1l, o2l] = r.rate([t1l, t2l], {score:score});
				resolve();
			}
		});
		promise.then(function() {
			outMessage+=`\n\n[Team1]`;
			for(let i=0;i<l;i++){
				p1 = t1[i];
				l1 = o1r[i];

				outMessage +=`\n${players[p1].nickname}: `;
				placed = (players[p1].leagues[leagueid].wins+players[p1].leagues[leagueid].losses+players[p1].leagues[leagueid].draws)-placementgames;
				outMessage+=u.stringUpdateRank(players[p1].leagues[leagueid].rank, l1, placed);

				players[p1].leagues[leagueid].rank = l1;
				players[p1].leagues[regionid].rank = o1r[i];
			}
			outMessage+=`\n[Team2]`;
			for(let i=0;i<l;i++){
				p2 = t2[i];
				l2 = o2r[i];

				outMessage +=`\n${players[p2].nickname}: `;
				placed = (players[p2].leagues[regionid].wins+players[p2].leagues[regionid].losses+players[p2].leagues[regionid].draws)-placementgames;
				outMessage+=u.stringUpdateRank(players[p2].leagues[leagueid].rank, l2, placed);

				players[p2].leagues[leagueid].rank = l2;
				players[p2].leagues[regionid].rank = o2r[i];
			}
			ranks = [t1r,t2r, t1l,t2l];
			d.reportMatch(match, s_totalgames, s_draws, ranks, leagueid, regionid);
			u.reply(message, outMessage, `Ended Match ${m}, result: ${result}`, true);
		});
	}else {
		d.reportMatch(match, s_totalgames, s_draws);
		u.reply(message, outMessage, `Ended Match ${m}, result: ${result}`, true);
	}

	for(let i=0;i<t1.length;i++){
		p1 = t1[i];
		p2 = t2[i];
		if(guild.active.hasOwnProperty(p1)) delete guild.active[p1];
		if(guild.active.hasOwnProperty(p2)) delete guild.active[p2];
	}
	delete guild.pickups[p].matches[m];
}

function setmatch(guild, message, matchid, result){
	regionid = guild.regionid; //we use regionid to find leagueid
	placementgames = guild.placementgames;
	d.setMatch(matchid, result,function(obj){
		if(obj.message!==""){
			u.reply(message, obj.message);
			return;
		}
		t1 = obj.t1;
		t2 = obj.t2;
		length = t1.length;
		rows = obj.rows;
		ranks = {};
		teams = {};
		for(row in rows){
			x = rows[row];
			ident = `${x.discordid}${x.leagueid}`;
			team = 0;
			index = t1.indexOf(x.discordid);
			if(index<0){
				team=1;
				index = t2.indexOf(x.discordid);
			}
			ranks[ident] = {
				matchid: matchid,
				discordid: x.discordid,
				leagueid: x.leagueid,
				wins: x.wins,
				losses: x.losses,
				draws: x.draws,
				team: team,
				index: index,
				mu: x.mu-x.muchange,
				sig: x.sig-x.sigchange,
				muchange: 0,
				sigchange: 0
			}
			switch(obj.oldresult){
				case "1":
					if(team==0) ranks[ident].wins--;
					else ranks[ident].losses--;
					break;
				case "2":
					if(team==1) ranks[ident].wins--;
					else ranks[ident].losses--;
					break;
				case "d":
					ranks[ident].draws--;
					break;
			}
			if(result=='c') {
				d.setMatchRanks(ranks[ident]);
				continue;
			}
			if (!(teams.hasOwnProperty(x.leagueid))){
				teams[x.leagueid] = [new Array(length), new Array(length)];
			}
			teams[x.leagueid][team][index] = new r.rating({mu:ranks[ident].mu, sigma:ranks[ident].sig});
		}
		if(result=='c') {
			u.reply(message, `Match ${matchid} Canceled.\nYour elos are as they were (I can't be fucked to post them LOLLOLOLO)`, `Undid Match ${matchid}`, true);
			return;
		}
		outteams = {};
		score = [0,1];
		if(result=='1') score = [1,0];
		for(t in teams){
			if(result=='d'){//it doesn't like draws with score [0,0] for some reason, need openskill to patch
				outteams[t] = r.rate(teams[t], {score:[1,1]});
			}else{
				outteams[t] = r.rate(teams[t], {score:score});
			}
		}
		outMessage = `Changed Match state to ${result}\nRank changes are as follows:\n\n`;
		for(row in rows){
			x = rows[row];
			ident = `${x.discordid}${x.leagueid}`;
			index = ranks[ident].index;
			team = ranks[ident].team;
			switch(result){
				case "1":
					if(team==0) ranks[ident].wins++;
					else ranks[ident].losses++;
					break;
				case "2":
					if(team==1) ranks[ident].wins++;
					else ranks[ident].losses++;
					break;
				case "d":
					ranks[ident].draws++;
					break;
			}
			totalgames = ranks[ident].wins+ranks[ident].losses+ranks[ident].draws;
			oldmu = ranks[ident].mu;
			numu = outteams[x.leagueid][team][index].mu;
			ranks[ident].muchange = numu - ranks[ident].mu;
			ranks[ident].mu = numu;
			nusig = outteams[x.leagueid][team][index].sigma;
			ranks[ident].sigchange = nusig - ranks[ident].sig;
			ranks[ident].sig = nusig;
			if(ranks[ident].leagueid!==regionid) outMessage+=`${ranks[ident].discordid}: ${u.stringUpdateRank(oldmu, numu, totalgames-placementgames)}\n`;
			d.setMatchRanks(ranks[ident]);
		}
		u.reply(message, outMessage, `Setmatch ${matchid} to ${result}`, true);
	});
}

module.exports = {
	addQueue,
	printQueue,
	removeQueue,
	printMatches,
	printMatch,
	report
}
