const { Client, Intents } = require('discord.js');
const cfg = require('./config.json');
const { Guild} = require('./structs.js');
const u = require('./utils.js');
const d = require('./dbreqs.js');
const mm = require('./matchmaker.js');
const a = require('./admin.js');
const rs = require("./ranksys.js");

const readline = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout
});

// Create a new client instance
const client = new Client({ 
	intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_MESSAGES
	] });

const prefix = "!";
var stats = {};
var guilds = {};
var activeChannels = [];

// When the client is ready, run this code (only once)
client.once('ready', () => {
	d.initStats(function(err,row) {
		stats = row;
		console.log(stats);
	});
	d.initGuilds(function(err, r){// the r here is a row from the guilds table
		guilds[r.guildid] = new Guild(r);
		activeChannels = activeChannels.concat(guilds[r.guildid].channelids);
	}, function(err, r) {//the r here is a row from the PICKUPS table
		a.addPickup(guilds[r.discordid], r);
	}, function(err, r){
		console.log("   ###   Online!   ###");
		for(var k in cfg.guilds){
			gid = cfg.guilds[k];
			console.log(""+k+": "+gid);
			for(var p in guilds[gid].pickups){
				console.log("   "+guilds[gid].pickups[p].name+": "+p);
			}
		}
	});
});

client.on("messageCreate", message => {
	if (message.author.bot) return;
	if (!(activeChannels.includes(message.channelId))) {
		if(message.content === "!pickups_enable"){
			//mguildId to escape naming issues
			mguildId = message.guildId;
			overlord = message.author.id;
			channelId = message.channelId;
			guilds[mguildId] = a.createGuild(mguildId,overlord,channelId,stats.nextleagueid);
			activeChannels.push(channelId);
			d.createGuild(mguildId,overlord,channelId.toString(),stats.nextleagueid++);
			u.reply(message, "Pickups enabled on this channel (but you still need to set them up)", "Enabled pickups");
		}
		return;
	}
	const guildId = message.guildId;
	if (!message.content.startsWith(prefix)){
		//Fast commands (no prefix)
		argz = message.content.split(' '); //WITH A Z on argz
		switch(message.content){
			case "++":
				if(mm.addQueue(guilds[guildId],message,message.author.id, [argz], stats.nextmatchid)) d.incMatchid(++stats.nextmatchid);
				break;
			case "--":
				mm.removeQueue(guilds[guildId],message, message.author.id, argz);
				break;
			default:
				break;
		}
		return;
	}
	const commandBody = message.content.slice(prefix.length);
	const args = commandBody.split(' ');
	const command = args[0].toLowerCase();

	switch(command){
		case "ping":
			const timeTaken = Date.now() - message.createdTimestamp;
			u.reply(message,`latency: ${timeTaken}ms.`);
			break;
		case "add":case "q":
			if(mm.addQueue(guilds[guildId],message,message.author.id, [args], stats.nextmatchid)) d.incMatchid(++stats.nextmatchid);
			break;
		case "remove":
			mm.removeQueue(guilds[guildId],message, message.author.id, argz);
			break;
		case "who":
			mm.printQueue(guilds[guildId],message);
			break;
		case "matches":
			mm.printMatches(guilds[guildId],message);
			break;
		case "match":case "teams":case "map":
			mm.printMatch(guilds[guildId],message,args);
			break;
		case "rl":
			mm.report(guilds[guildId], message, 'l'); // lucy in the
			break;
		case "rc":
			mm.report(guilds[guildId], message, 'c'); // cky with
			break;
		case "rd":
			mm.report(guilds[guildId], message, 'd'); // diamonds
			break;
		case "setdefault": case "set":
			if(args.length<3) u.reply(message, "its !setdefault <setting> <value>.")
			else a.set(guilds[guildid],args[1],args[2], function(out){
				u.reply(message, out);
			});
			break;
		case "maps":
			u.reply(message, `[${guilds[guildId].mappool}]`);
			break;
		case "createpickup": case "addpickup":
			if(args.length<2) u.reply(message, "its !createpickup <name>");
			else {
				name = args[1];
				pickupid = stats.nextpickupid++;
				leagueid = stats.nextleagueid++;
				guilds[guildId].pickups[pickupid] = a.createPickup(name, pickupid, guildId, leagueid, guilds[guildId]);
				d.createPickup(name, pickupid, guildId, leagueid, stats);
				u.reply(message,`Pickup ${name} created with id:${pickupid}`);
			}
			break;
		case "rank": case "player":
			if(args.length>1){
				novalue = "";

				max = false;
				current = true;
				player = "";
				global = false;
				alltime = false;
				region = ""
				discordid = "";
				leagueid = "";
				season = "";


				[flags, inputs] = u.cmdArgs(args,1);
				inc = 0; //INput Counter
				for(let i=0;i<flags.length;i++){
					switch(flags[i]){
						case 'm': case 'max':
							max = true;
							break;
						case 'c': case "current":
							current = true;
							break;
						case 'g': case "global":
							global = true;
							break;
						case 'a': case "alltime":
							alltime = true;
							break;
						case 'p': case "player":
							if(inc<inputs.length) player=inputs[inc++];
							else novalue = flags[i];
							break;
						case 'r': case "region":
							if(inc<inputs.length) region=inputs[inc++];
							else novalue = flags[i];
							break;
						case 'd': case "discordid":
							if(inc<inputs.length) {
								if(parseInt(inputs[inc])==NaN){
									novalue = "NaN";
									inc++;//redundant?
								}
								else discordid=inputs[inc++];
							}
							else novalue = flags[i];
							break;
						case 'l': case "leagueid":
							if(inc<inputs.length) {
								if(parseInt(inputs[inc])==NaN){
									novalue = "NaN";
									inc++;//redundant?
								}
								else leagueid=inputs[inc++];
							}
							else novalue = flags[i];
							break;
						case 's': case "season":
							if(inc<inputs.length) season=inputs[inc++];
							else novalue = flags[i];
							break;
						default:
							u.reply(message, "Not a valid arguement");
							return;
							break;
					}
					if(novalue=="NaN"){
						u.reply(message, "\""+flags[i]+"\" flag requires a number input");
						return;
					}else if(novalue!=""){
						u.reply(message, "\""+novalue+"\" flag used but no value provided");
						return;
					}
				}
				if(inc<inputs.length && player=="") player=inputs[inc++];
				//TODO ranked system no arguements setup, always set to display seasonal rank as of current
				rs.getRank(guilds[guildId],message,true,player);
			}else rs.getRank(guilds[guildId],message, true);
			break;
		case "lb": case "leaderboard":
			leagueid = null;
			for(p in guilds[guildId].pickups){
				pickup = guilds[guildId].pickups[p];
				leagueid = pickup.leagueid;
			}
			if(leagueid==null){
				u.reply(message, "ERROR, Notify developers code:bignuts");
				//if you hit this error, its probably because you are trying to view leaderboard but no pickups exist. This code should just be replaced, because you should be able to access other leaderboards from other servers
				return;
			}

			if(args.length>1){
				novalue = "";

				max = false;
				current = true;
				page = 1;
				global = false;
				alltime = false;
				region = ""
				season = "";

				[flags, inputs] = u.cmdArgs(args,1);
				inc = 0; //INput Counter
				for(let i=0;i<flags.length;i++){
					switch(flags[i]){
						case 'm': case 'max':
							max = true;
							break;
						case 'c': case "current":
							current = true;
							break;
						case 'p': case "page":
							if(inc<inputs.length) {
								if(parseInt(inputs[inc])==NaN){
									novalue = "NaN";
									inc++;//redundant?
								}
								else page=inputs[inc++];
							}
							else novalue = flags[i];
							break;
						case 'g': case "global":
							global = true;
							break;
						case 'a': case "alltime":
							alltime = true;
							break;
						case 'r': case "region":
							if(inc<inputs.length) region=inputs[inc++];
							else novalue = flags[i];
							break;
						case 'l': case "leagueid":
							if(inc<inputs.length) {
								if(parseInt(inputs[inc])==NaN){
									novalue = "NaN";
									inc++;//redundant?
								}
								else leagueid=inputs[inc++];
							}
							else novalue = flags[i];
							break;
						case 's': case "season":
							if(inc<inputs.length) season=inputs[inc++];
							else novalue = flags[i];
							break;
						default:
							u.reply(message, "\""+flags[i]+"\" is not a valid flag");
							return;
							break;
					}
					if(novalue=="NaN"){
						u.reply(message, "\""+flags[i]+"\" flag requires a number input");
						return;
					}else if(novalue!=""){
						u.reply(message, "\""+novalue+"\" flag used but no value provided");
						return;
					}
				}
				if(inc<inputs.length && page==1 && !isNaN(Number(inputs[inc]))) page=inputs[inc++];
				//TODO ranked system no arguements setup, always set to display seasonal rank as of current
				rs.getLB(message, leagueid, page);
			}else rs.getLB(message, leagueid);

			break;
		case "noadds": case "bans":
			if(args.length>1) a.listNoadds(message, guildId, parseInt(args[1]));
			else a.listNoadds(message, guildId);
			break;
		case "setmatch": case "rw":
			if (args.length<3){
				u.reply(message, "format: !setmatch <matchid> <1|2|d|c>");
				return;
			}else if(isNaN(parseInt(args[1]))){
				u.reply(message, "matchid is supposed to be a number");
				return;
			}
			switch(args[2]){
				case '1':case '2': case 'd': case 'c':
					mm.setmatch(guilds[guildId], message, args[1], args[2]);
					return;
					break;
			}
			u.reply(message, "State must be '1', '2', 'd', or 'c'");
			break;
		case "cfg": case "config":
			a.printConfig(guilds[guildId],message);
			break;
		default:return;
	}

});

//directly type commands into console, good for testing/debugging
readline.on('line', (line) => {
	args=line.split(' ');
	switch(args[0]){
		case "stats":console.log(stats);break;
		case "guilds":console.log(cfg.guilds);break;
		case "activeChannels":case "activechannels":case "activechans":
			console.log(activeChannels);break;
		case "guild":
			if(args.length<2) console.log("need to specify which guild");
			else if(cfg.guilds.hasOwnProperty(args[1])){
				console.log(guilds[cfg.guilds[args[1]]]);
			}else if(guilds.hasOwnProperty(args[1])){
				console.log(guilds[args[1]]);
			}else console.log("not a valid guild");
			break;
		case "pickups":
			for(var k in cfg.guilds){
				gid = cfg.guilds[k];
				console.log(""+k+": "+gid);
				for(var p in guilds[gid].pickups){
					console.log("   "+guilds[gid].pickups[p].name+": "+p);
				}
			}
			break;
		case "active":
			for(var k in cfg.guilds){
				gid = cfg.guilds[k];
				console.log(""+k+": "+gid);
				console.log(guilds[gid].active);
			}
			break;
		case "tree":
			for(var k in cfg.guilds){
				gid = cfg.guilds[k];
				console.log(""+k+": "+gid);
				for(var p in guilds[gid].pickups){
					console.log("  "+guilds[gid].pickups[p].name+": "+p);
					console.log("    In Queue: "+guilds[gid].pickups[p].queue.length);
					console.log("    Matches:");
					matches = guilds[gid].pickups[p].matches;
					for(var m in matches){
						console.log("      "+m+": "+matches[m].state);
					}
				}
			}
			break;
		default:console.log("console command not recognised");break;
	}
});

readline.on("close", () => {
	d.close((e) => {
		if(!e) {
			client.destroy();//due to non-sequential order of js, im gonna say that this never actually gets ran
			process.exit(0);
		}
		else{
			console.log(e);
			client.destroy();
			process.exit(1);
		}
	});
});


// Login to Discord with your client's token
client.login(cfg.token);

