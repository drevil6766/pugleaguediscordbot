var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('maindb.sqlite3');

function initStats(cb){
	db.get("SELECT * FROM stats", cb);
}

function initGuilds(cb1, cb2, cbr){
	db.each("SELECT * FROM discords", cb1, function(er, rows){
		//console.log(`Loaded ${rows} Guilds`);
		db.each("SELECT * FROM pickups", cb2, cbr);
	});
}

function createGuild(guildId, overlord, channelId, regionId){
	db.run("INSERT OR REPLACE INTO discords(guildid, overlordid, channelids, regionid) VALUES ((?), (?), (?), (?))", guildId, overlord, channelId.toString(), regionId);
	db.run("UPDATE stats SET nextleagueid = ?", regionId);
	db.run("INSERT OR REPLACE INTO leagues(id, name) VALUES (?, ?)", regionId, guildId);
}

//callsback with true if it did
function setGuild(guildId, setting, value, cb) {
	let getSuccess = new Promise(function(resolve,reject) {
		switch(setting){
			case 'regionid':
				/*
				db.serialize(function() {
					db.run(`UPDATE discords SET regionid = ? WHERE 
					( (SELECT locked FROM leagues WHERE id = ?) = 0) 
					AND guildid = ?`, value, value, guildId);
					db.get("SELECT COUNT(*) FROM discords WHERE (guildid = ?) AND (regionid = ?)", guildId, value, function(err, r) {
						cb(r[`COUNT(*)`]==1);
					});
				});
				return false; break;*/
				resolve();break;
			case "whiterole": case "blackrole":
				db.run("UPDATE discords SET ? = ? WHERE guildid = ?", setting, value, guildId, resolve());
				break;
			case 'mappool':
				db.run("UPDATE discords SET mappool = ? WHERE guildid = ?", value, guildId, resolve());
				break;
			case 'pickteams':
				db.run("UPDATE discords SET pickteams = ? WHERE guildid = ?", value, guildId, resolve());
				break;
			case 'teamsize':
				db.run("UPDATE discords SET teamsize = ? WHERE guildid = ?", value, guildId, resolve());
				break;
			case "ranked":
				db.run("UPDATE discords SET ranked = ? WHERE guildid = ?", value, guildId, resolve());
				break;
			default:
				resolve(); break;
		}
	});
	getSuccess.then(function(){
		//should be able to do SELECT ? (setting) but for some reason that won't work, as it tries to grab a literal "?" property from discords
		db.get("SELECT * FROM discords WHERE (guildid = ?)", guildId, function(err, r){
			cb(r[setting]==value);
		});
	});
}

function createPickup(name, pickupid, discordid, leagueid, stats) {
	db.run("INSERT OR REPLACE INTO pickups(name,pickupid,discordid,leagueid) VALUES (?, ?, ?, ?)", name, pickupid, discordid, leagueid);
	db.run("INSERT OR REPLACE INTO leagues(id, name) VALUES (?, ?)", stats.nextleagueid, `Pickup ${pickupid} initial season`);
	db.run("UPDATE stats SET nextleagueid = ?, nextpickupid = ?", stats.nextleagueid+1, stats.nextpickupid+1);
}

function reportMatch(match, totalgames, draws, ranks=null, leagueid=null, regionid=null){
	matchid = match.matchId;
	pickupid = match.pickupId;
	result = match.result;
	starttime = match.startTime;
	team1list = "";
	team2list = "";
	updateranks = (result!=='c');
	for(let i=0;i<match.t1.length;i++){
		p1 = match.t1[i]
		p2 = match.t2[i]
		db.run("INSERT OR REPLACE INTO players(discordid, nickname) VALUES (?, ?)", p1, match.players[p1].nickname);
		db.run("INSERT OR REPLACE INTO players(discordid, nickname) VALUES (?, ?)", p2, match.players[p2].nickname);
		team1list+=p1;
		team2list+=p2;
		if(i<match.t1.length-2){
			team1list+=",";
			team2list+=",";
		}
		if(updateranks){
			//ill be honest, this shit looks ugly. However, to make it look nice, it would be a waste of memory and speed. In the end the computer wins, and this code looks horrible
			r1 = match.players[p1].leagues[regionid];
			l1 = match.players[p1].leagues[leagueid];
			r1mc = r1.rank.mu - ranks[0][i].mu;
			r1sc = r1.rank.sigma - ranks[0][i].sigma;
			l1mc = l1.rank.mu - ranks[2][i].mu;
			l1sc = l1.rank.sigma - ranks[2][i].sigma;
			db.run("INSERT OR REPLACE INTO ranks(discordid, leagueid, mu, sig, wins, losses, draws, streak) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",p1, regionid, r1.rank.mu, r1.rank.sigma, r1.wins, r1.losses, r1.draws, r1.streak); 
			db.run("INSERT OR REPLACE INTO elochanges(matchid, discordid, leagueid, muchange, sigchange, classes) VALUES (?, ?, ?, ?, ?, ?)", matchid, p1, regionid, r1mc, r1sc, null); 
			db.run("INSERT OR REPLACE INTO ranks(discordid, leagueid, mu, sig, wins, losses, draws, streak) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",p1, leagueid, l1.rank.mu, l1.rank.sigma, l1.wins, l1.losses, l1.draws, l1.streak); 
			db.run("INSERT OR REPLACE INTO elochanges(matchid, discordid, leagueid, muchange, sigchange, classes) VALUES (?, ?, ?, ?, ?, ?)", matchid, p1, leagueid, l1mc, l1sc, null); 
			r2 = match.players[p2].leagues[regionid];
			l2 = match.players[p2].leagues[leagueid];
			r2mc = r2.rank.mu - ranks[1][i].mu;
			r2sc = r2.rank.sigma - ranks[1][i].sigma;
			l2mc = l2.rank.mu - ranks[3][i].mu;
			l2sc = l2.rank.sigma - ranks[3][i].sigma;
			db.run("INSERT OR REPLACE INTO ranks(discordid, leagueid, mu, sig, wins, losses, draws, streak) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",p2, regionid, r2.rank.mu, r2.rank.sigma, r2.wins, r2.losses, r2.draws, r2.streak); 
			db.run("INSERT OR REPLACE INTO elochanges(matchid, discordid, leagueid, muchange, sigchange, classes) VALUES (?, ?, ?, ?, ?, ?)", matchid, p2, regionid, r2mc, r2sc, null); 
			db.run("INSERT OR REPLACE INTO ranks(discordid, leagueid, mu, sig, wins, losses, draws, streak) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",p2, leagueid, l2.rank.mu, l2.rank.sigma, l2.wins, l2.losses, l2.draws, l2.streak); 
			db.run("INSERT OR REPLACE INTO elochanges(matchid, discordid, leagueid, muchange, sigchange, classes) VALUES (?, ?, ?, ?, ?, ?)", matchid, p2, leagueid, l2mc, l2sc, null); 
		}
	}

	db.run("INSERT OR REPLACE INTO matches(matchid, pickupid, team1list, team2list, result, starttime) VALUES (?, ?, ?, ?, ?, ?)", matchid, pickupid, team1list, team2list, result, starttime);

	//db.run("UPDATE pickups SET s_totalgames = ?, set s_draws = ? WHERE pickupid = ?", [totalgames, draws, pickupid]);
	// It seems that it doesn't like UPDATE statements with more than 2 parameters
	db.run("UPDATE pickups SET s_totalgames = ? WHERE pickupid = ?", totalgames, pickupid);
	db.run("UPDATE pickups SET s_draws = ? WHERE pickupid = ?", draws, pickupid);

}

function getRank(authorId, leagueId, cb) {
	db.get("SELECT mu, sig, wins, losses, draws, streak FROM ranks WHERE discordid = ? AND leagueid = ?", authorId, leagueId, cb);
}

function getLB(leagueid, placementgames, page, cb){
	db.all("SELECT ranks.discordid, players.nickname, ranks.mu, ranks.sig, ranks.wins, ranks.losses, ranks.draws FROM ranks JOIN players ON ranks.discordid = players.discordid WHERE leagueid = ? AND (wins+losses+draws) > ? ORDER BY ranks.mu desc LIMIT 10 OFFSET ?", leagueid, placementgames, (page-1)*10, cb);
}

function getDetailedRank(authorid, player, leagueid, placementgames, cb){
	//getid
	//getrank
	//if they have finished placements, then get their spot on lb if in top500
	db.serialize(function() {
		pid = "";
		let getpid = new Promise(function(resolve,reject) {
			if (player===""){
				pid = authorid;
				resolve();
			}else db.get("SELECT discordid FROM players WHERE nickname = ?", player, function(err, row) {
				if((typeof row)=='undefined') return;
				pid = row.discordid;
				resolve();
			});
		});
		getpid.then(function() {
			if(pid ==""){
				cb();
				return;
			}
			getRank(pid, leagueid, function(err, row) {
				getLB(leagueid, placementgames, 50, function(forget, lb) {
					let promise = new Promise(function(resolve,reject) {
						for(i in lb){
							if(lb[i].discordid==pid){
								row.position = parseInt(i)+1;
								break;
							}
						}
						resolve();
					});
					promise.then(function() {
						cb(row);
					});
				});
			});
		});
	});
}

function incMatchid(nextmatchid){
	db.run("UPDATE stats SET nextmatchid = ?", nextmatchid);
}

function getInfractions(guildid, page, bans, cb){
	if(bans) db.all("SELECT infractions.*, players.nickmame FROM infractions JOIN players on infractions.discordid = players.discordid WHERE discordid = ? AND infractions.expired = 0 AND infractions.duration < 0 ORDER BY infractions.issuetime asc LIMIT ?", guildid, (page+1)*10, cb);
	else db.all("SELECT infractions.*, players.nickmame FROM infractions JOIN players on infractions.discordid = players.discordid WHERE discordid = ? AND infractions.expired = 0 AND (infractions.issuetime+infractions.duration < ?) ORDER BY infractions.issuetime asc LIMIT ?", guildid, Date.now(), (page+1)*10, cb);
}

function setMatch(matchid, result, cb){
	db.get("SELECT matches.*, pickups.leagueid FROM matches JOIN pickups ON pickups.pickupid = matches.pickupid WHERE matchid = ?", matchid, function(er, row){
		if(row===undefined){
			cb({message:`No match with id ${matchid} found`});
		}else if (result==row.result){
			cb({message:`That match already has that result`});
		}else if (row.result=='c'){
			cb({message:`Cannot change canceled matched`});
		}
		db.run("UPDATE matches SET result = ? WHERE matchid = ?", result, matchid);
		db.all("SELECT elochanges.*, ranks.* FROM elochanges JOIN ranks ON elochanges.leagueid = ranks.leagueid AND elochanges.discordid = ranks.discordid WHERE matchid = ?", matchid, function(er2, rows) {
			t1 = row.team1list.split(',');
			t2 = row.team2list.split(',');
			cb({t1:t1,t2:t2,rows:rows,message:"",oldresult:row.result});
		});
	});
}

function setMatchRanks(obj){
	db.run("UPDATE elochanges SET muchange = ?, sigchange = ? WHERE matchid = ? AND leagueid = ? AND discordid = ?", obj.muchange, obj.sigchange, obj.matchid, obj.leagueid, obj.discordid);
	db.run("UPDATE ranks SET mu = ?, sig = ?, wins = ?, losses = ?, draws = ? WHERE discordid = ? AND leagueid = ?", obj.mu, obj.sig, obj.wins, obj.losses, obj.draws, obj.discordid, obj.leagueid);
}

function close(cb) {db.close(cb);}

module.exports = {
	initStats,
	initGuilds,
	createGuild,
	setGuild,
	createPickup,
	reportMatch,
	getRank,
	getLB,
	getDetailedRank,
	incMatchid,
	getInfractions,
	setMatch,
	setMatchRanks,
	close
}
