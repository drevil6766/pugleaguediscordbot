// Communications

function reply(message, out,log="",send=false){
	did = message.guildId;
	did = did.substr(did.length - 3);
	if(send){
		message.channel.send(out)
			.then(message => { 
				if(log==="nolog") return;
				else console.log(`${did} Send: ${log}`)
			})
			.catch(console.error);
	} else{
		message.reply(out)
			.then(message => { 
				if(log==="nolog") return;
				else console.log(`${did} Send: ${log}`)
			})
			.catch(console.error);
	}
}

function dm(message, out, log = '') {
	did = message.author.id;
	did = did.substr(did.length - 3);
	message.author
		.send(out)
		.then((message) => {
			if (log === 'nolog') return;
			else console.log(`${did} Send: ${log}`);
		})
		.catch(console.error);
}

// String Manipulations

function stringRank(rank, group=true) {
	//TODO sigma bias
	//TODO groups
	return `${Math.round(rank.mu*100)}`;
}

function stringUpdateRank(before, after, placed){
	//-1 = not placed
	// 0 = JUST placed
	// 1 = been placed a while mate
	outMessage = "";
	if(placed==0) outMessage = `<< Placed: ${stringRank(after)} >>`;
	else if(placed<0) outMessage = "unplaced";
	else outMessage = `${stringRank(before)} --> ${stringRank(after)}`;
	//TODO: what if you get promoted?
	//if promoted then add on to the end:
	// | Promoted to <rank>
	return outMessage;
}

function printLB(rows, page){
	if(typeof rows == 'undefined') return "Empty Leaderboard";
	outMessage = "```markdown\n";
	outMessage+= "  # |  LOC  |   Rating   |         Name         | Matches | W/L (Win%)\n";
	outMessage+= "=======================================================================\n";
	for(let i=0;i<rows.length;i++){
		tgames = rows[i].wins+rows[i].losses+rows[i].draws;
		pos = (page-1)*10+i+1;
		outMessage+= `${fitString(pos,3,1)} | [ ? ] | ${fitString(stringRank(rows[i]),10,-1)} |${fitString(rows[i].nickname,22)}|  ${fitString(tgames,5,1)}  | ${rows[i].wins}/${rows[i].losses} (${Math.round(100*(rows[i].wins/(rows[i].wins+rows[i].losses)))}%)\n`;
	}
	outMessage+="```";
	return outMessage;
}

//fits a string into a set number of characters
function fitString(s, size, align=0){
	s = ''+s;
	if(s.length==size) return s;
	if(s.length>size) return s.substring(0,size);
	if(align<0){
		return s+(" ".repeat(size-s.length));

	}else if(align>0){
		return " ".repeat(size-s.length)+s;
	}else{
		lower = Math.floor((size-s.length)/2);
		higher = size-s.length-lower
		return " ".repeat(lower)+s+(" ".repeat(higher));
	}
}

function printNoadds(rows, number){
	if(typeof rows == 'undefined') return "No noadds";
	if((number-1)*10>=rows.length) return "Not that many pages";
	outMessage = "```markdown\n";
	for(let i=(number-1)*10;i<rows.length;i++){
		howLong="";
		if(rows[i].duration<0) howLong = "forever";
		else howLong = toTimeString(rows[i].duration);
		outMessage+=`${rows[i].nickname}(${rows[i].discordid}) | @: ${Date(rows[i].issuetime)} | For: ${howLong} | Why: ${rows[i].reason} | By: ${rows[i].issuer}\n`;
	}
	outMessage+="```";
	return outMessage;
}

function toTimeString(s){
	mins = Math.floor(s/60);
	if(mins>60){
		hours = Math.floor(mins/60);
		if(hours>24){
			days = Math.floor(hours/24);
			return `${days}d${hours}h${mins}m`;
		}else return `${hours}h${mins}m`;
	}else return `${mins}m`;
}

function matchString(match, simple=false){
	t1names = new Array(match.t1.length);
	t2names = new Array(match.t2.length);
	for(let i=0;i<match.t1.length;i++){
		t1names[i] = match.players[match.t1[i]].nickname;
		t2names[i] = match.players[match.t2[i]].nickname;
	}
	outMessage = `Match ${match.matchId}:`;
	if(simple) outMessage+=` [${t1names}] vs [${t2names}]`;
	else{
		outMessage+=`\nTeam1:\n[${t1names}]\n\nTeam2:\n[${t2names}]`;
		if(match.map) outMessage+= `\n\nMap: ${match.map}`;
	}
	return outMessage;
}

// utilities

function randInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function cmdArgs(args,skip=0){
	//skip = what index in args to start on (generally the 1st arg is the fucntion, which we don't want to deciper)
	flgArr = []; //flag array
	inpArr = []; //input array
	for(let i=skip;i<args.length;i++){
		if(args[i].charAt(0)=='-'){
			if(args[i].charAt(1)=='-')flgArr.push(args[i].slice(2));
			else{
				for(let j=1;j<args[i].length;j++){
					flgArr.push(args[i].charAt(j));
				}
			}
		}else inpArr.push(args[i]);
	}
	return [flgArr,inpArr];
}


module.exports = {
	reply,
	dm,
	randInt,
	stringUpdateRank,
	stringRank,
	printLB,
	printNoadds,
	cmdArgs,
	matchString
}

